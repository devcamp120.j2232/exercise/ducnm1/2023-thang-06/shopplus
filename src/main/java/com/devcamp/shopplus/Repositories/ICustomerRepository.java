package com.devcamp.shopplus.Repositories;

import com.devcamp.shopplus.Entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {

}
