package com.devcamp.shopplus.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shopplus.Entities.Order;

public interface IOrderRepository extends JpaRepository<Order, Integer> {

}
