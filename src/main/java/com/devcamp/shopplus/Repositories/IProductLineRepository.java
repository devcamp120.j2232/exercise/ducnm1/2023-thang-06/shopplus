package com.devcamp.shopplus.Repositories;

import com.devcamp.shopplus.Entities.ProductLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductLineRepository extends JpaRepository<ProductLine, Integer> {

}
