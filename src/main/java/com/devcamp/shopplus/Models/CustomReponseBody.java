package com.devcamp.shopplus.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomReponseBody {
    private String message;
    private Object body;

    public CustomReponseBody(String message) {
        this.message = message;
    }
}
