package com.devcamp.shopplus.Services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entities.Customer;
import com.devcamp.shopplus.Repositories.ICustomerRepository;

@Service
public class CustomerService {
    // create customer service class that will handle 6 methods: getAllCustomers,
    // getCustomerById, createCustomer, updateCustomer, deleteCustomer,
    // deleteAllCustomers
    // getAllCustomers: return all customers
    // getCustomerById: return customer by id
    // createCustomer: create new customer
    // updateCustomer: update customer by id
    // deleteCustomer: delete customer by id
    // deleteAllCustomers: delete all customers

    @Autowired
    private ICustomerRepository customerRepository;

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(int id) {
        return customerRepository.findById(id).orElse(null);
    }

    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(int id, Customer customer) {
        Customer existingCustomer = customerRepository.findById(id).orElse(null);
        /*
         * public class Customer {
         * 
         * @Id
         * 
         * @GeneratedValue(strategy = GenerationType.IDENTITY)
         * private int id;
         * 
         * //lastName
         * 
         * @Column(name = "last_name", nullable = false, length = 50)
         * private String lastName;
         * 
         * //firstName
         * 
         * @Column(name = "first_name", nullable = false, length = 50)
         * private String firstName;
         * 
         * // phoneNumber
         * 
         * @Column(name = "phone_number", nullable = false, length = 50)
         * private String phoneNumber;
         * 
         * // address
         * 
         * @Column(name = "address", nullable = false, length = 255)
         * private String address;
         * 
         * // city
         * 
         * @Column(name = "city", nullable = false, length = 50)
         * private String city;
         * 
         * // state
         * 
         * @Column(name = "state", nullable = false, length = 50)
         * private String state;
         * 
         * // postalCode
         * 
         * @Column(name = "postal_code", nullable = false, length = 50)
         * private String postalCode;
         * 
         * // country
         * 
         * @Column(name = "country", nullable = false, length = 50)
         * private String country;
         * 
         * // salesRepEmployeeNumber
         * 
         * @Column(name = "sales_rep_employee_number", nullable = true, length = 10)
         * private int salesRepEmployeeNumber;
         * 
         * // creditLimit
         * 
         * @Column(name = "credit_limit", nullable = true, length = 10)
         * private int creditLimit;
         * 
         * // payments
         * 
         * @OneToMany
         * private List<Payment> payments;
         * }
         */
        existingCustomer.setLastName(customer.getLastName());
        existingCustomer.setFirstName(customer.getFirstName());
        existingCustomer.setPhoneNumber(customer.getPhoneNumber());
        existingCustomer.setAddress(customer.getAddress());
        existingCustomer.setCity(customer.getCity());
        existingCustomer.setState(customer.getState());
        existingCustomer.setPostalCode(customer.getPostalCode());
        existingCustomer.setCountry(customer.getCountry());
        existingCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
        existingCustomer.setCreditLimit(customer.getCreditLimit());
        existingCustomer.setPayments(customer.getPayments());

        return customerRepository.save(existingCustomer);
    }

    public String deleteCustomer(int id) {
        customerRepository.deleteById(id);
        return "Customer removed !! " + id;
    }

    public String deleteAllCustomers() {
        customerRepository.deleteAll();
        return "All customers removed !!";
    }

}
