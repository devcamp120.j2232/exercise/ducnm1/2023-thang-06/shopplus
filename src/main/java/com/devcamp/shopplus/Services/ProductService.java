package com.devcamp.shopplus.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shopplus.Entities.Product;
import com.devcamp.shopplus.Repositories.IProductLineRepository;
import com.devcamp.shopplus.Repositories.IProductRepository;

@Service
public class ProductService {
    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IProductLineRepository productLineRepository;

    // get all products
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    // get product by id
    public Product getProductById(int id) {
        return productRepository.findById(id).orElse(null);
    }

    // create product
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    // update product
    /*
     * public class Product {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * // productCode
     * 
     * @Column(name = "product_code", unique = true, nullable = false, length = 50)
     * private String productCode;
     * 
     * // productName
     * 
     * @Column(name = "product_name", nullable = true, length = 255)
     * private String productName;
     * 
     * // productDescription
     * 
     * @Column(name = "product_description", nullable = true, length = 2500)
     * private String productDescription;
     * 
     * // productLine
     * 
     * @ManyToOne
     * 
     * @JoinColumn(name = "product_line_id", unique = true, nullable = false)
     * private ProductLine productLine;
     * 
     * // productScale
     * 
     * @Column(name = "product_scale", nullable = true, length = 50)
     * private String productScale;
     * 
     * // productVendor
     * 
     * @Column(name = "product_vendor", nullable = true, length = 50)
     * private String productVendor;
     * 
     * // quantityInStock
     * 
     * @Column(name = "quantity_in_stock", nullable = true)
     * private int quantityInStock;
     * 
     * // buyPrice
     * 
     * @Column(name = "buy_price", nullable = true)
     * private double buyPrice;
     * 
     * // orderDetail
     * 
     * @OneToMany(mappedBy = "product")
     * private List<OrderDetail> orderDetails;
     * 
     * }
     */
    public Product updateProduct(int id, Product product) {
        Product existProduct = productRepository.findById(id).orElse(null);
        existProduct.setProductCode(product.getProductCode());
        existProduct.setProductName(product.getProductName());
        existProduct.setProductDescription(product.getProductDescription());
        existProduct.setProductLine(productLineRepository.findById(product.getProductLine().getId()).orElse(null));
        existProduct.setProductScale(product.getProductScale());
        existProduct.setProductVendor(product.getProductVendor());
        existProduct.setQuantityInStock(product.getQuantityInStock());
        existProduct.setBuyPrice(product.getBuyPrice());
        return productRepository.save(existProduct);
    }

    // delete product by id
    public String deleteProductById(int id) {
        productRepository.deleteById(id);
        return "Product " + id + " was deleted";
    }

    // delete all products
    public String deleteAllProducts() {
        productRepository.deleteAll();
        return "All products were deleted";
    }
}
