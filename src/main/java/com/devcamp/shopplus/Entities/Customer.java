package com.devcamp.shopplus.Entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "customers")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //lastName
    @Column(name = "last_name", nullable = false, length = 50)
    private String lastName;

    //firstName
    @Column(name = "first_name", nullable = false, length = 50)
    private String firstName;

    // phoneNumber
    @Column(name = "phone_number", nullable = false, length = 50)
    private String phoneNumber;

    // address
    @Column(name = "address", nullable = false, length = 255)
    private String address;

    // city
    @Column(name = "city", nullable = false, length = 50)
    private String city;

    // state
    @Column(name = "state", nullable = false, length = 50)
    private String state;

    // postalCode
    @Column(name = "postal_code", nullable = false, length = 50)
    private String postalCode;

    // country
    @Column(name = "country", nullable = false, length = 50)
    private String country;

    // salesRepEmployeeNumber
    @Column(name = "sales_rep_employee_number", nullable = true, length = 10)
    private int salesRepEmployeeNumber;

    // creditLimit
    @Column(name = "credit_limit", nullable = true, length = 10)
    private int creditLimit;

    // payments
    @OneToMany
    private List<Payment> payments;
}
