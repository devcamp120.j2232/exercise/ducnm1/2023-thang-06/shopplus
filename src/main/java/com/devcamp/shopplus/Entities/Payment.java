package com.devcamp.shopplus.Entities;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "payments")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // customer
    @ManyToOne
    @JoinColumn(name = "customer_id", unique = true, nullable = false)
    private Customer customer;

    // checkNumber
    @Column(name = "check_number", nullable = false, length = 50)
    private String checkNumber;

    // paymentDate
    @Column(name = "payment_date", nullable = false)
    private Date paymentDate;

    // amount
    @Column(name = "amount", nullable = false)
    private double amount;
}
