package com.devcamp.shopplus.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "offices")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // city
    @Column(name = "city", nullable = false, length = 50)
    private String city;

    // phone
    @Column(name = "phone", nullable = false, length = 50)
    private String phone;

    // addressLine
    @Column(name = "address_line", nullable = false, length = 255)
    private String addressLine;

    // state
    @Column(name = "state", nullable = true, length = 50)
    private String state;

    // country
    @Column(name = "country", nullable = false, length = 50)
    private String country;

    // territory
    @Column(name = "territory", nullable = false, length = 50)
    private String territory;

}
