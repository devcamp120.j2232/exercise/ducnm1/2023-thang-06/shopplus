package com.devcamp.shopplus.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "order_details")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // order
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    // product
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    // quantityOrdered
    @Column(name = "quantity_ordered", nullable = false)
    private int quantityOrdered;

    // priceEach
    @Column(name = "price_each", nullable = false)
    private double priceEach;

}
