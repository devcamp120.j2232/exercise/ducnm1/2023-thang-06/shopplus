package com.devcamp.shopplus.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employees")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // lastName
    @Column(name = "last_name", nullable = false, length = 50)
    private String lastName;

    // firstName
    @Column(name = "first_name", nullable = false, length = 50)
    private String firstName;

    // extension
    @Column(name = "extension", nullable = true, length = 50)
    private String extension;

    // email
    @Column(name = "email", unique = true, nullable = true, length = 100)
    private String email;

    // officeCode
    @Column(name = "office_code", nullable = true, length = 10)
    private int officeCode;

    // reportsTo
    @Column(name = "reports_to", nullable = true, length = 10)
    private int reportsTo;

    // jobTitle
    @Column(name = "job_title", nullable = true, length = 50)
    private String jobTitle;
}
