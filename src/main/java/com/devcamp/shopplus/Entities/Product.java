package com.devcamp.shopplus.Entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "products")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // productCode
    @Column(name = "product_code", unique = true, nullable = false, length = 50)
    private String productCode;

    // productName
    @Column(name = "product_name", nullable = true, length = 255)
    private String productName;

    // productDescription
    @Column(name = "product_description", nullable = true, length = 2500)
    private String productDescription;

    // productLine
    @ManyToOne
    @JoinColumn(name = "product_line_id", unique = true, nullable = false)
    private ProductLine productLine;

    // productScale
    @Column(name = "product_scale", nullable = true, length = 50)
    private String productScale;

    // productVendor
    @Column(name = "product_vendor", nullable = true, length = 50)
    private String productVendor;

    // quantityInStock
    @Column(name = "quantity_in_stock", nullable = true)
    private int quantityInStock;

    // buyPrice
    @Column(name = "buy_price", nullable = true)
    private double buyPrice;

    // orderDetail
    @OneToMany(mappedBy = "product")
    private List<OrderDetail> orderDetails;

}
