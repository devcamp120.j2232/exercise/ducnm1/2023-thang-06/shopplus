package com.devcamp.shopplus.Entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "product_lines")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "product_line", unique = true, nullable = false, length = 50)
    private String productLine;

    @Column(name = "description", nullable = true, length = 2500)
    private String description;

    // products
    @OneToMany(mappedBy = "productLine")
    private List<Product> products;
}
