package com.devcamp.shopplus.Entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "orders")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // orderDate
    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    // requiredDate
    @Column(name = "required_date", nullable = false)
    private Date requiredDate;

    // shippedDate
    @Column(name = "shipped_date", nullable = true)
    private Date shippedDate;

    // status
    @Column(name = "status", nullable = false, length = 50)
    private String status;

    // comments
    @Column(name = "comments", nullable = true, length = 255)
    private String comments;

    // customer
    @ManyToOne
    @JoinColumn(name = "customer_id", unique = true, nullable = false)
    private Customer customer;

    // orderDetail
    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails;
}
